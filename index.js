const express=require('express')
const bodyParser=require('body-parser')
const cors=require('cors')
const routeRegister=require('./routes/jwtAuth')

const app=express()

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}))


app.use('/auth',routeRegister)


app.listen(5000,()=>{
    console.log("server started and running")
})