const express=require('express')
const bcrypt=require('bcrypt')
const pool = require('../db')
const jwtGenerator=require('../utils/jwtGenerator')

const router=express.Router()


router.post('/register',async(req,res)=> {
    try{
 
        const {name,email,password}=req.body


        const user=await pool.query("SELECT * FROM users WHERE user_email=$1",[
            email
        ]);

        if(user.rows.length!==0) {
            return res.status(401).send("user already exits")
        }

        else {
        const saltRound=10
        const salt=await bcrypt.genSalt(saltRound);

        const bcryptPassword=await bcrypt.hash(password,salt)

        const newUser=await pool.query("INSERT INTO users(user_name,user_email,user_password) VALUES ($1,$2,$3) RETURNING *",[name,email,bcryptPassword]);

      const token=jwtGenerator(newUser.rows[0].user_id)

      res.json({ token:token });
        }
         
    }
    catch(err) {
    console.error(err.message)
    res.status(500).send('server error');


    
    }
})


router.post('/login',async (req,res)=>{
    try {
       const {email,password}=req.body

       const user=await pool.query("SELECT * FROM users WHERE user_email=$1",[email])
    
       if(user.rows.length===0) {
        return res.json({message:"email or password is incorrect"})
    }

    const validPassword=await bcrypt.compare(password,user.rows[0].user_password)
    
    console.log(validPassword)

    
    if(!validPassword) {
        res.send("password incorrect")
    }

    const token=jwtGenerator(user.rows[0].user_id)

    res.send(token)


    }




    catch(err) {
        console.log(err.message)
    }
})


module.exports=router